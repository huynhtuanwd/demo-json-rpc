Demo của json-rpc-2.0 api
JSON-RPC là 1 giao thức RPC, đặc điểm chính của nó là định nghĩa một vài cấu trúc dữ liệu và quy tắc trong quá trình xử lý của nó. Giao thức này
hỗ trợ việc thực hiện JSON-RPC 1.0, JSON-RPC 2.0, Batch requests, HTTP, Websocket.

JSON-RPC khá giống với chuẩn REST, tuy nhiên nó chỉ có 1 method POST, dễ dàng chuyển, chia sẽ thông tin với giao thức khác HTTP/S, WebSockets, XMPP, telnet, SFTP, SCP, or SSH
JSON-RPC test trên các ngôn ngữ nhanh hơn REST về tốc độ truyền data.

Json-rpc-2.0 api:
Đối tượng Request (jsonrpc version, method, params(optional), id(optional))
Nếu ko có ai thì Response sẽ ko có content
Đối tượng Reponse (jsonrpc version, result, error, id)

Hướng dẫn demo của json-rpc-2.0 api.
Installation:
npm install
npm start

Call JSON-RPC-API in postman:
Domain :http://localhost:3000
Method :POST
Function Get info: data: {"jsonrpc": "2.0","method": "getInfo","id": 100}
Function Sum two numbers: data: {"jsonrpc": "2.0","method": "getInfo",params:[5,10],"id": 100}
